import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import axios from 'axios';
import marked from 'marked';
import * as removeMd from 'remove-markdown';

createApp(App).use(router, axios, marked, removeMd).mount('#app')
